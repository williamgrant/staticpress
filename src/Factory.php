<?php
namespace StaticPress;

use Kurenai\Parser;
use Kurenai\Parsers\Content\MarkdownParser;
use Kurenai\Parsers\Metadata\YamlParser;
use Pimple\Container;
use StaticPress\Twig\AssetExtension;
use StaticPress\Twig\StrExtension;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;
use Twig_Loader_Filesystem;

final class Factory
{
    public static function newContainer($overrides = [])
    {
        return new Container(array_merge([
            'config' => function () {
                $configFile = str_replace('%base_path%', getcwd(), file_get_contents(getcwd() . '/config.yml'));

                return collect(array_dot(Yaml::parse($configFile)));
            },
            'parser' => function () {
                return new Parser(new YamlParser, new MarkdownParser);
            },
            'twig' => function ($c) {
                $twig = new Twig_Environment(new Twig_Loader_Filesystem($c['config']->get('paths.layouts')), [
                    'debug' => false,
                ]);

                // Add Twig extensions
                $twig->addExtension(new AssetExtension());
                $twig->addExtension(new StrExtension());

                return $twig;
            },
        ], $overrides));
    }
}