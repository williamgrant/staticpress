<?php
namespace StaticPress\Concerns;

use StaticPress\Tasks\BuildAssetsTask;

trait BuildsAssets
{
    /**
     * @param string $path
     * @return \StaticPress\Tasks\BuildAssetsTask
     */
    public function taskBuildAssets($path)
    {
        return new BuildAssetsTask($path);
    }
}