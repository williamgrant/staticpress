<?php
namespace StaticPress\Concerns;

use StaticPress\Tasks\BuildRssTask;

trait BuildsRss
{
    /**
     * @param string $path
     * @return \StaticPress\Tasks\BuildRssTask
     */
    public function taskBuildRss($path)
    {
        return new BuildRssTask($path);
    }
}