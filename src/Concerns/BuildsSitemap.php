<?php
namespace StaticPress\Concerns;

use StaticPress\Tasks\BuildSitemapTask;

trait BuildsSitemap
{
    /**
     * @param string $path
     * @return \StaticPress\Tasks\BuildSitemapTask
     */
    public function taskBuildSitemap($path)
    {
        return new BuildSitemapTask($path);
    }
}