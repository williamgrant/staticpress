<?php
namespace StaticPress\Concerns;

use StaticPress\Tasks\BuildApiTask;

trait BuildsApi
{
    /**
     * @param string $path
     * @return \StaticPress\Tasks\BuildApiTask
     */
    public function taskBuildApi($path)
    {
        return new BuildApiTask($path);
    }
}