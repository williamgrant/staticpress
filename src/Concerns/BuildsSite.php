<?php
namespace StaticPress\Concerns;

use StaticPress\Tasks\BuildSiteTask;

trait BuildsSite
{
    /**
     * @param string $path
     * @return \StaticPress\Tasks\BuildSiteTask
     */
    public function taskBuildSite($path)
    {
        return new BuildSiteTask($path);
    }
}