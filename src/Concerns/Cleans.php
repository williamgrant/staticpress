<?php
namespace StaticPress\Concerns;

use StaticPress\Tasks\CleanTask;

trait Cleans
{
    /**
     * @param string $path
     * @return \StaticPress\Tasks\CleanTask
     */
    public function taskClean($path)
    {
        return new CleanTask($path);
    }
}