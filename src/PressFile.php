<?php
namespace StaticPress;

use Robo\Common\TaskIO;
use Robo\Tasks;
use StaticPress\Concerns\BuildsAssets;
use StaticPress\Concerns\BuildsRss;
use StaticPress\Concerns\BuildsApi;
use StaticPress\Concerns\BuildsSite;
use StaticPress\Concerns\BuildsSitemap;
use StaticPress\Concerns\Cleans;

class PressFile extends Tasks
{
    use BuildsApi;
    use BuildsAssets;
    use BuildsRss;
    use BuildsSite;
    use BuildsSitemap;
    use Cleans;
    use TaskIO;

    protected $buildDir = '';

    public function __construct()
    {
        $this->container = ContainerFactory::getStaticInstance();

        $this->buildDir = $this->container['config']->get('paths.build');
    }

    public function press()
    {
        $this->printTaskInfo('Starting build process');

        // Purge old builds
        $this->clean();

        // Build static HTML pages
        $this->pressHtml();

        // Compile and copy assets over to the build folder
        $this->pressAssets();

        $this->printTaskSuccess('Congratulations, your build was successful!');
    }

    public function serve()
    {
        $this
            ->taskServer(3000)
            ->dir($this->buildDir)
//            ->background()
            ->run();
    }

    public function watch()
    {
        $config = $this->container['config'];

        $this
            ->taskWatch()
            ->monitor([
                $config->get('paths.assets'),
                $config->get('paths.content'),
                $config->get('paths.layouts'),
            ], function () {
                $this->say('Changes detected');
                $this->press();
            })
            ->run();
    }

    public function clean()
    {
        return $this->taskClean($this->buildDir)->run();
    }

    protected function pressHtml()
    {
        $config = $this->container['config'];
        $content = $this->container['content']->collect();

        $this
            ->taskBuildSite($content)
            ->target($this->buildDir)
            ->run();

        if ($config->get('sitemap.enabled')) {
            $this->pressSitemap($content);
        }

        if ($config->get('rss.enabled')) {
            $this->pressRss($content);
        }

        if ($config->get('api.enabled')) {
            $this->pressApi($content);
        }
    }

    public function pressAssets()
    {
        $config = $this->container['config'];

        $this
            ->taskBuildAssets($config->get('paths.assets'))
            ->target($this->buildDir . '/dist')
            ->run();
    }

    public function pressApi($content = null)
    {
        $config = $this->container['config'];
        $content = $content ? $content : $this->container['content']->collect();

        $this
            ->taskBuildApi($config->get('api.filename') . '.json')
            ->with($content)
            ->target($this->buildDir)
            ->run();
    }

    public function pressRss($content = null)
    {
        $config = $this->container['config'];
        $content = $content ? $content : $this->container['content']->collect();

        $this
            ->taskBuildRss($config->get('rss.filename') . '.xml')
            ->with($content)
            ->target($this->buildDir)
            ->run();
    }

    public function pressSitemap($content = null)
    {
        $config = $this->container['config'];
        $content = $content ? $content : $this->container['content']->collect();

        $this
            ->taskBuildSitemap($config->get('sitemap.filename'))
            ->with($content)
            ->target($this->buildDir)
            ->run();
    }
}