<?php
namespace StaticPress\Tasks;

use Robo\Common\TaskIO;
use Robo\Tasks;
use StaticPress\Factory;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use SitemapPHP\Sitemap;
use Suin\RSSWriter\Channel;
use Suin\RSSWriter\Feed;
use Suin\RSSWriter\Item;

class BuildTask extends Tasks
{
    use TaskIO;

    public function __construct()
    {
        $this->container = Factory::newContainer();
    }

    public function build($opts = ['serve' => false, 'watch' => false])
    {
        $config = $this->container['config'];
        $fs = new Filesystem();

        // Purge the old build
        $fs->remove($config->get('paths.build'));

        $this->buildHtml();
        $this->buildAssets();

        if ($opts['serve']) {
            $this->serve();
        }

        if ($opts['watch']) {
            $this->watch();
        }
    }

    public function buildHtml()
    {
        $config = $this->container['config'];
        $fs = new Filesystem();

        // Parse all documents in the content directory
        $content = $this->collectContent();
        $twig = $this->container['twig'];
        $twigData = [
            'content' => $content,
            'categories' => $content->pluck('category')->unique()->filter(),
            'tags' => $content->pluck('tags')->flatten()->values()->unique()->filter(),
        ];

        // Write pages and furnish each with a reference to the pages collection
        $finder = new Finder();
        $finder->files()->in($config->get('paths.content'));
        foreach ($content as $pageData) {
            $fs->dumpFile(
                $config->get('paths.build') . '/' . $pageData['uri'],
                $twig->render($pageData['layout'] . '.twig', array_merge($twigData, $pageData))
            );
        }

        $this->say('Generated static HTML');

        if ($config->get('sitemap.enabled')) {
            $this->buildSitemap($content);
        }

        if ($config->get('rss.enabled')) {
            $this->buildRss($content);
        }

        if ($config->get('api.enabled')) {
            $this->buildApi($content);
        }
    }

    public function buildAssets()
    {
        $config = $this->container['config'];
        $fs = new Filesystem();
        $finder = new Finder();

        $finder->files()->in($config->get('paths.assets'));

        // Copy our assets over to the build directory
        foreach ($finder as $file) {
            $fs->copy(
                $file->getPathName(),
                $config->get('paths.build') . '/dist/' . $file->getRelativePathname()
            );
        }

        $this->say('Compiled assets');
    }

    public function buildApi($content = null)
    {
        if ( ! $content) {
            $content = $this->collectContent();
        }

        $config = $this->container['config'];
        $fs = new Filesystem();

        $json = collect([
            'content' => $content->toArray(),
            'categories' => $content->pluck('category')->unique()->filter(),
            'tags' => $content->pluck('tags')->flatten()->values()->unique()->filter(),
        ])->toJson();

        $fs->dumpFile(
            $config->get('paths.build') . '/' . $config->get('api.filename') . '.json',
            $json
        );

        $this->say('Added API endpoint');
    }

    public function buildRss($content = null)
    {
        if ( ! $content) {
            $content = $this->collectContent();
        }

        $config = $this->container['config'];
        $fs = new Filesystem();

        // Initialise an RSS feed
        $feed = new Feed();
        $channel = new Channel();

        $channel
            ->title($config->get('title'))
            ->description($config->get('description'))
            ->url($config->get('url'))
            ->language('en-GB')
            ->copyright('Copyright 2012, Foo Bar')
            ->pubDate(strtotime('Tue, 21 Aug 2012 19:50:37 +0900'))
            ->lastBuildDate(strtotime('Tue, 21 Aug 2012 19:50:37 +0900'))
            ->ttl(60)
            ->appendTo($feed);

        // Add pages to RSS
        foreach ($content as $page) {
            $item = new Item();
            $item
                ->title($page['title'])
                ->description("<div>Blog body</div>")
                ->url($page['uri'])
                ->pubDate(strtotime('Tue, 21 Aug 2012 19:50:37 +0900'))
                ->guid($page['uri'], true)
                ->appendTo($channel);
        }

        // Write out the RSS feed
        $fs->dumpFile(
            $config->get('paths.build') . '/' . $config->get('rss.filename') . '.xml',
            $feed
        );

        $this->say('Added RSS feed');
    }

    public function buildSitemap($content = null)
    {
        if ( ! $content) {
            $content = $this->collectContent();
        }

        $config = $this->container['config'];

        // Initialise the sitemap
        $sitemap = new Sitemap($config->get('url'));
        $sitemap
            ->setPath($config->get('paths.build') . '/')
            ->setFilename($config->get('sitemap.filename'));

        // Add each page
        foreach ($content as $page) {
            $sitemap->addItem($page['uri']);
        }

        // Finalise the generated sitemap
        $sitemap->createSitemapIndex($config->get('url') . '/', 'Today');

        $this->say('Generated sitemap');
    }

    public function serve()
    {
        $config = $this->container['config'];

        $this->taskExec('php -S localhost:3000 -t ' . $config->get('paths.build'))->background()->run();
    }

    public function watch()
    {
        $config = $this->container['config'];

        $this
            ->taskWatch()
            ->monitor([
                $config->get('paths.assets'),
            ], function() {
                $this->buildAssets();
            })
            ->monitor([
                $config->get('paths.content'),
                $config->get('paths.layouts'),
            ], function() {
                $this->buildHtml();
            })
            ->run();
    }

    private function collectContent()
    {
        $config = $this->container['config'];
        $parser = $this->container['parser'];

        $finder = new Finder();
        $finder->files()->in($config->get('paths.content'));

        // Parse all documents in the content directory
        $content = collect([]);
        foreach ($finder as $file) {
            $parsedFile = $parser->parse($file->getPathName());

            $layoutData = $parsedFile->getMetadata();
            $layoutData['body'] = $parsedFile->getContent();
            $layoutData['uri'] = '/' . str_replace('.md', '.html', $file->getRelativePathname());

            $content->push($layoutData);
        }

        return $content;
    }
}