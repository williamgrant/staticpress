<?php
namespace StaticPress\Tasks;

use Robo\Contract\TaskInterface;
use Robo\Result;
use StaticPress\ContainerFactory;

class CleanTask implements TaskInterface
{
    /**
     * @var \Pimple\Container
     */
    protected $container;

    /**
     * @var string
     */
    protected $path;

    public function __construct($path)
    {
        $this->container = ContainerFactory::getStaticInstance();
        $this->path = $path;
    }

    public function run()
    {
        $this->container['fs']->remove($this->path);

        return Result::success($this, 'Cleaned build directory');
    }
}