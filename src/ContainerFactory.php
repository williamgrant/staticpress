<?php
namespace StaticPress;

use Mni\FrontYAML\Parser;
use Pimple\Container;
use StaticPress\Twig\AssetExtension;
use StaticPress\Twig\IlluminateStringExtension;
use StaticPress\Yaml\YamlParser;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Yaml\Yaml;
use Twig_Environment;
use Twig_Extension_StringLoader;
use Twig_Loader_Filesystem;

final class ContainerFactory
{
    public static $container;

    public static function getStaticInstance()
    {
        if ( ! self::$container instanceof Container) {
            $container = new Container();

            $container['config'] = function () {
                $configFile = str_replace('%base_path%', getcwd(), file_get_contents(getcwd() . '/config.yml'));

                return collect(array_dot(Yaml::parse($configFile)));
            };

            $container['content'] = $container->factory(function ($c) {
                return new ContentRepository($c);
            });

            $container['fs'] = $container->factory(function ($c) {
                return new Filesystem();
            });

            $container['parser'] = function () {
                return new Parser();
            };

            $container['twig'] = $container->factory(function ($c) {
                $twig = new Twig_Environment(new Twig_Loader_Filesystem($c['config']->get('paths.layouts')), [
                    'debug' => false,
                ]);

                // Add the content path to Twig loader (used in parsing of meta data)
                $twig->getLoader()->addPath($c['config']->get('paths.content'));

                // Add Twig extensions
                $twig->addExtension(new AssetExtension());
                $twig->addExtension(new IlluminateStringExtension());
                $twig->addExtension(new Twig_Extension_StringLoader());

                return $twig;
            });

            $container['finder'] = $container->factory(function ($c) {
                return new Finder();
            });

            self::$container = $container;
        }

        return self::$container;
    }
}