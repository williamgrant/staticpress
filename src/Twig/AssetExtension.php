<?php
namespace StaticPress\Twig;

use Twig_Extension;
use Twig_SimpleFilter;

class AssetExtension extends Twig_Extension
{
    public function getName()
    {
        return 'AssetExtension';
    }

    public function getFilters()
    {
        return [
            new Twig_SimpleFilter('asset', function ($path) {
                return '/dist/' . ltrim($path, '/');
            }),
        ];
    }
}